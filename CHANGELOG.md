Changelog

## :rocket: Release [v1.0.10](https://gitlab.com/stands2/templates/service-template/compare/v1.0.9...v1.0.10) (2022-10-05)
          

### :bug: Bug Fixes

* **deps:** update dependency amqplib to ^0.10.0 ([f6c8359](https://gitlab.com/stands2/templates/service-template/commit/f6c83595bb3073997b3a88a1012b0bac12fb99ae))
* **deps:** update dependency ioredis to v5 ([8f02df9](https://gitlab.com/stands2/templates/service-template/commit/8f02df94bbacc43f4041c1863bfb2d5877cd1747))


### :recycle: Chores

* **cleaning:** remove dist folder from repo ([b4df253](https://gitlab.com/stands2/templates/service-template/commit/b4df253095e946632fc5c9ad5b3ead529b15a119))
* **common:** update naming after moving repo ([e044aab](https://gitlab.com/stands2/templates/service-template/commit/e044aab60790468672ca46fc8b911b5ce8db1206))
* **deps:** add renovate.json ([2d27b6e](https://gitlab.com/stands2/templates/service-template/commit/2d27b6e76bcafa3a41bd59a29bd22fd1e85ab7df))
* **deps:** pin dependencies ([4463a32](https://gitlab.com/stands2/templates/service-template/commit/4463a3297b6d0e17d65e02aaa7fe0b7ba366e399))
* **deps:** update dependency dotenv to v16.0.3 ([3a695e9](https://gitlab.com/stands2/templates/service-template/commit/3a695e9c81d98bf578180863dbbacf8741960248))
* **deps:** update dependency eslint to v8.24.0 ([84ab0bf](https://gitlab.com/stands2/templates/service-template/commit/84ab0bfd83892abd685301a0c8e881cfd1d1ffb2))
* **deps:** update dependency typescript to v4.8.4 ([7a23815](https://gitlab.com/stands2/templates/service-template/commit/7a23815b8e26a80a0ffa24f99b6abe258eb63aa2))
* **deps:** update docker docker tag to v20.10.18 ([dbdddb8](https://gitlab.com/stands2/templates/service-template/commit/dbdddb8a62f0ff80980e3af4ff80b78abea3fcd0))
* **deps:** update typescript-eslint monorepo to v5.38.1 ([c283e0e](https://gitlab.com/stands2/templates/service-template/commit/c283e0e2d8bb7b1f6664e7af51e57d4672940605))
* **deps:** update typescript-eslint monorepo to v5.39.0 ([55b3d12](https://gitlab.com/stands2/templates/service-template/commit/55b3d1219a5f4d88902cec3daca9d316cd00beb3))
* **docker:** update docker-compose.dev.yml for local docker development ([48596e8](https://gitlab.com/stands2/templates/service-template/commit/48596e823695066854f55e003d4e2d8e4b2b35c9))

## :rocket: Release [v1.0.9](https://gitlab.com/stands2/backend/template/compare/v1.0.8...v1.0.9) (2022-09-26)
          

### :bug: Bug Fixes

* **release:** fix pathUrl in elevently config ([396817e](https://gitlab.com/stands2/backend/template/commit/396817e8f38d1881b50d41185a62d14d0f4d1213))

## :rocket: Release [v1.0.8](https://gitlab.com/stands2/backend/template/compare/v1.0.7...v1.0.8) (2022-09-26)
          

### :bug: Bug Fixes

* **release:** fix url of deployed documentation ([27b8935](https://gitlab.com/stands2/backend/template/commit/27b8935133863382ad758cc191445b3afa023909))

## :rocket: Release [v1.0.7](https://gitlab.com/stands2/backend/template/compare/v1.0.6...v1.0.7) (2022-09-26)
          

### :bug: Bug Fixes

* **release:** fix base url deployed pages and clean ci ([f49936e](https://gitlab.com/stands2/backend/template/commit/f49936e6ccb588e810f472fa9d0bc8da77cb51ab))

## :rocket: Release [v1.0.6](https://gitlab.com/stands2/backend/template/compare/v1.0.5...v1.0.6) (2022-09-26)
          

### :bug: Bug Fixes

* **release:** fix coverage report and skipped status main branch ([61a7e50](https://gitlab.com/stands2/backend/template/commit/61a7e50be94d143c20a7d4eb632b4463b9d11a98))

## :rocket: Release [v1.0.5](https://gitlab.com/stands2/backend/template/compare/v1.0.4...v1.0.5) (2022-09-26)
          

### :bug: Bug Fixes

* **release:** fix release stage in ci ([131dde9](https://gitlab.com/stands2/backend/template/commit/131dde994a9fa0a34c1ba5c94d8c35c7d7940948))

## :rocket: Release [v1.0.4](https://gitlab.com/stands2/backend/template/compare/v1.0.3...v1.0.4) (2022-09-26)
          

### :bug: Bug Fixes

* **release:** fix release stage in ci ([2534678](https://gitlab.com/stands2/backend/template/commit/2534678fc6ce79a99c7ce361f30f4b939de7b8d1))

## :rocket: Release [v1.0.3](https://gitlab.com/stands2/backend/template/compare/v1.0.2...v1.0.3) (2022-09-26)
          

### :bug: Bug Fixes

* **release:** fix the release workflow on main ([0971d32](https://gitlab.com/stands2/backend/template/commit/0971d32d699c416ea567d1cade02bdd8a692aeac))

## :rocket: Release [v1.0.2](https://gitlab.com/stands2/backend/template/compare/v1.0.1...v1.0.2) (2022-09-26)
          

### :bug: Bug Fixes

* **release:** fix the release workflow on main ([9c7e9d8](https://gitlab.com/stands2/backend/template/commit/9c7e9d8d5c353c7ebe4165057ecf85838d49e674))

## :rocket: Release [v1.0.1](https://gitlab.com/stands2/backend/template/compare/v1.0.0...v1.0.1) (2022-09-26)
          

### :bug: Bug Fixes

* **release:** fix the release workflow on main ([ad7160b](https://gitlab.com/stands2/backend/template/commit/ad7160bb0b4fef1ddfd9c51e906ce1846b2b7a55))
* **release:** fix the release workflow on main ([e07a470](https://gitlab.com/stands2/backend/template/commit/e07a470b8f6b4650864dd0b20cccc88ac95d5d3d))

# :rocket: Release  v1.0.0 (2022-09-25)
          

### :memo: Documentation

* **readme:** update Readme ([e18aac9](https://gitlab.com/stands2/backend/template/commit/e18aac9ea5f9c0849da2a633852393a00af6d442))


### :robot: Continuous Integration

* **deps:** Add Dependencies scanning ([419647e](https://gitlab.com/stands2/backend/template/commit/419647efab1b22339d48b2d3451edd6a201288cf))
* **docker:** add docker image build and push to image registry ([648114c](https://gitlab.com/stands2/backend/template/commit/648114c045fe9aa20f68b7e06598179b601ad8fd))
* **documentation:** add release job for the documentation ([40fcbcd](https://gitlab.com/stands2/backend/template/commit/40fcbcdaf95c7272e67d222a974f9ac7ab5d206b))
* **release:** add release step in CI ([95384d7](https://gitlab.com/stands2/backend/template/commit/95384d7c841e15412a135cb9a21e06d543618934))
* **release:** fix publish CI job ([356bcc9](https://gitlab.com/stands2/backend/template/commit/356bcc980f7035d073e77f94dc2cc2628d292d7f))


### :recycle: Chores

* **licence:** add GPL GNU v3 Licence ([b5f8605](https://gitlab.com/stands2/backend/template/commit/b5f8605fb075d9a489cd0f2d5a1ba9c818a08181))


### :heavy_check_mark: Tests

* **tests:** add more basic tests ([0d0eee1](https://gitlab.com/stands2/backend/template/commit/0d0eee15ced1716a81b3403a94a4a48d4af67646))


### :sparkles: Enhancements

* **release:** add ci release automation ([6d08b8f](https://gitlab.com/stands2/backend/template/commit/6d08b8f148d0de5e73db29284ed5662a9a2ba82a))
