const eleventyNavigationPlugin = require('@11ty/eleventy-navigation');

module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(eleventyNavigationPlugin);
  // eleventyConfig.ignores.add("coverage");
  // eleventyConfig.ignores.add("docs");
  eleventyConfig.addPassthroughCopy('README.md');
  eleventyConfig.addPassthroughCopy('coverage');
  eleventyConfig.addPassthroughCopy('docs');

  return {
    pathPrefix: '/templates/service-template',
    dir: {
      input: '_static',
      output: 'public',
      // includes: "../../_static"
    },
  };
};
