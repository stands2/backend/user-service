import {
  Service,
  type ServiceBroker,
  type Context,
  type ActionHandler,
  type ServiceEventHandler,
} from 'moleculer';
import type { ActionWelcomeCtx } from '../models';

class TemplateService extends Service {
  /**
   * Creates an instance of TemplateService.
   * @param {ServiceBroker} broker
   * @memberof TemplateService
   */
  public constructor(public broker: ServiceBroker) {
    super(broker);
    this.parseServiceSchema({
      name: 'template',
      // dependencies: [
      // 	"auth",
      // 	"users"
      // ],
      /**
       * Service settings
       */
      settings: {},

      /**
       * Service dependencies
       */
      // Add - dependencies: [],

      /**
       * Actions
       */
      actions: {
        /**
         * Say a 'Hello' action.
         *
         */
        hello: {
          handler: this.ActionHello,
        },

        /**
         * Welcome, a username
         */
        welcome: {
          // rest: '/welcome',
          cache: {
            keys: ['name'],
          },
          params: {
            name: 'string',
          },
          handler: this.ActionWelcome,
          // async handler(ctx: Context<{ name: string }>): Promise<string> {
          //   return this.ActionWelcome(ctx);
          // },
        },
      },

      /**
       * Events
       */
      events: {
        'user.created': this.userCreated,
      },

      /**
       * Methods
       */
      methods: {
        sum: this.sumMethod,
      },

      /**
       * Service created lifecycle event handler
       */
      created: this.serviceCreated,

      /**
       * Service started lifecycle event handler
       */
      started: this.serviceStarted,

      /**
       * Service stopped lifecycle event handler
       */
      stopped: this.serviceStopped,
    });
  }

  /**
   * Action handler
   *
   * @return {*}  {string}
   * @memberof TemplateService
   */
  private ActionHello: ActionHandler<string> = async (): Promise<string> => {
    return 'Hello Moleculer';
  };

  /**
   * Action handler
   *
   * @param {string} name
   * @return {*}  {string}
   * @memberof TemplateService
   */
  private ActionWelcome: ActionHandler<string> = async (
    ctx: Context<ActionWelcomeCtx>
  ): Promise<string> => {
    ctx.emit('name.welcome', ctx.params.name);
    return this.sayWelcome(ctx.params.name);
  };

  // Private method
  private sayWelcome = (name: string) => {
    this.logger.info('Say hello to', name);
    return `Welcome, ${name}`;
  };

  /**
   * Event handler
   *
   * @param {string} user
   * @memberof TemplateService
   */
  private userCreated: ServiceEventHandler = async (
    ctx: Context<{ user: string; a: number; b: number }>
  ): Promise<void> => {
    this.sum(ctx.params.a, ctx.params.b);
    // this.broker.call('mail.send', { user: ctx.params.user });
  };

  private sumMethod = (a: number, b: number): number => {
    return a + b;
  };

  /**
   * Service created lifecycle event handler
   *
   * @memberof TemplateService
   */
  private serviceCreated = () => {
    this.logger.info('ES6 Service created');
  };

  /**
   * Service started lifecycle event handler
   *
   * @memberof TemplateService
   */
  private serviceStarted = () => {
    this.logger.info('ES6 Service started.');
  };

  /**
   * Service stopped lifecycle event handler
   *
   * @memberof TemplateService
   */
  private serviceStopped = () => {
    this.logger.info('ES6 Service stopped.');
  };
}

export { TemplateService };
export default TemplateService;
