FROM node:18-alpine as base
WORKDIR /usr/app
COPY package*.json ./
COPY tsconfig*.json ./

#
##
### image builder
FROM base as ts-compiler
RUN npm ci --ignore-scripts
COPY . ./
ENV NODE_ENV=production
RUN npm run build

#
##
### typescript files stripper
FROM node:18-alpine as ts-remover
WORKDIR /usr/app
COPY --from=ts-compiler /usr/app/package*.json ./
COPY --from=ts-compiler /usr/app/dist ./
ENV NODE_ENV=production
RUN npm ci --ignore-scripts

#
##
### production image light
FROM gcr.io/distroless/nodejs:18 as production
WORKDIR /usr/app
COPY --from=ts-remover /usr/app ./
USER 1000
ENV NODE_ENV=production
CMD ["./node_modules/.bin/moleculer-runner", "--config", "./moleculer.config.js"]
